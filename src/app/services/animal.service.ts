import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Food } from '../classes/food';
import { Dog } from '../classes/dog';
import { Cat } from '../classes/cat';

@Injectable()
export class AnimalService {

  public dirdog = '../../assets/dogs.json';
  public dircat = '../../assets/cats.json';

  constructor( private http: HttpClient ) { }

  public getPerros(): Observable<Dog[]> {
    return new Observable<Dog[]>(observe => {
      this.http.get(this.dirdog).subscribe((data: any[]) => {
        const perros = data.map(value => new Dog(value));
        observe.next(perros);
        observe.complete();
      }, error => {
        alert('Error al pedir los datos');
        observe.next(error);
        observe.complete();
      });
    });
  }

    public getGatos(): Observable<Cat[]> {
      return new Observable<Cat[]>(observe => {
        this.http.get(this.dircat).subscribe((data: any[]) => {
          const gatos = data.map(value => new Cat(value));
          observe.next(gatos);
          observe.complete();
        }, error => {
          alert('Error al pedir los datos');
          observe.next(error);
          observe.complete();
        });
      });
    }

      public getPerrosVerdes(): Observable<Dog[]> {
        return new Observable<Dog[]>(observe => {
          this.http.get(this.dirdog).subscribe((data: any[]) => {
            const perros = data.map(value => new Dog(value));
            const filtro = perros.filter(data => data.color === 'Green');
            observe.next(filtro);
            observe.complete();
          });
        });
      }

      
  public getComida(): Observable<Food[]> {
    return new Observable<Food[]>(observe => {
      this.http.get(this.dirdog).subscribe((data: any[]) => {
        const perros = data.map(value => new Dog(value));
        let array = [];
        let filtroArray = [];
        for(let perro of perros){
          for(let food of perro.food){
            array.push(food);
          }
        }
        filtroArray = this.MiFiltroArray(array, ident => ident.id)
        observe.next(filtroArray);
        observe.complete();
      
      });
    });
  }

  public MiFiltroArray(info, key){
    return [
      ...new Map( info.map(x => [key(x),x])).values()
    ]
  }
}
