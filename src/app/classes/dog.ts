import { Animal } from '../classes/animal';
import { Food } from '../classes/food';

export class Dog extends Animal{
    public food: Food[];
    public raza: string;
    public isMale: boolean;

    constructor(data: any){
        super(data);
        this.food = data.food.map(value => new Food(value));
        this.raza = data.raza;
        this.isMale = data.isMale;
    }
}
