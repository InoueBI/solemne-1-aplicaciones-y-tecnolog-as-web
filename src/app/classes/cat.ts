import { Animal } from '../classes/animal';

export class Cat extends Animal{
    public isFurry: boolean;
    public meows: boolean;

    constructor(data: any){
        super(data);
        this.isFurry = data.isFurry;
        this.meows = data.meows;
    }
}
