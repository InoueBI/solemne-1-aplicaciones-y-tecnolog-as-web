export class Animal {
    public id: number;
    public name: string;
    public color: string;
    
    constructor(data: any = null){
        this.update(data);
    }

    private update(data: any): void {
        if(data){
        this.id = data.id;
        this.name = data.name;
        this.color = data.color;
        }
    }
}