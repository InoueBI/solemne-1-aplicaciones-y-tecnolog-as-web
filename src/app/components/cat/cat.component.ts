import { Component, OnInit } from '@angular/core';
import { AnimalService } from 'src/app/services/animal.service';
import { Cat } from '../../classes/cat';

@Component({
  selector: 'app-cat',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.scss']
})
export class CatComponent implements OnInit {

  public gato: Cat[];

  constructor( private animalService: AnimalService ) { }

  ngOnInit(): void {
    this.animalService.getGatos().subscribe(data => {
      this.gato = data;
    });
    
  }

}
