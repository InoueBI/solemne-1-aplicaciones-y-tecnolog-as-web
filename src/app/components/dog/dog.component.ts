import { Component, OnInit } from '@angular/core';
import { AnimalService } from 'src/app/services/animal.service';
import { Dog } from '../../classes/dog';

@Component({
  selector: 'app-dog',
  templateUrl: './dog.component.html',
  styleUrls: ['./dog.component.scss']
})
export class DogComponent implements OnInit {

  public perro: Dog[];

  constructor( private animalService: AnimalService ) { }

  ngOnInit(): void {
    this.animalService.getPerros().subscribe(data => {
      this.perro = data;
    });
  }

}