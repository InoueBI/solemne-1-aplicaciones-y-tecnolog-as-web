import { Component, OnInit } from '@angular/core';
import { AnimalService } from 'src/app/services/animal.service';
import { Food } from 'src/app/classes/food';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss']
})
export class FoodComponent implements OnInit {

  public comida: Food[];
  
  constructor( private animalService: AnimalService ) { }

  ngOnInit(): void {
    this.animalService.getComida().subscribe(data => {
      this.comida = data.map(value => new Food(value));
    });
  }

}
