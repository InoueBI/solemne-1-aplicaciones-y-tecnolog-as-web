import { Component, OnInit } from '@angular/core';
import { AnimalService } from 'src/app/services/animal.service';
import { Dog } from '../../classes/dog';

@Component({
  selector: 'app-green-dog',
  templateUrl: './green-dog.component.html',
  styleUrls: ['./green-dog.component.scss']
})
export class GreenDogComponent implements OnInit {

  public perroVerde: Dog[];

  constructor( private animalService: AnimalService ) { }

  ngOnInit(): void {
    this.animalService.getPerrosVerdes().subscribe(data => {
      this.perroVerde = data;
    })
  }

}
