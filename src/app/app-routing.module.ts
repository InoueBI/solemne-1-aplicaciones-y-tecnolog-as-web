import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CatComponent } from './components/cat/cat.component';
import { DogComponent } from './components/dog/dog.component';
import { GreenDogComponent } from './components/green-dog/green-dog.component';
import { FoodComponent } from './components/food/food.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'gatos', component: CatComponent },
  { path: 'perros', component: DogComponent },
  { path: 'perros-verdes', component: GreenDogComponent },
  { path: 'comida', component: FoodComponent },
  { path: '**', pathMatch:'full', redirectTo: 'home' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
