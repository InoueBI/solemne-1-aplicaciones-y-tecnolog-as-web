# Agregando la imagen de ubuntu al docker
FROM ubuntu:18.04

# Agregando creador del dockerfile
MAINTAINER Tomás Barrueto

# Copiando toda la aplicacion en el docker
COPY . .

# Actualizando mi SO
RUN sudo apt-get update -y

# Instalando nodejs y npm en ubuntu
RUN sudo apt-get install nodejs -y
RUN sudo apt-get install npm -y

# Creando y cambiando de directorio a /app
WORKDIR /app

# Copiando todo en la nueva carpeta
COPY ./ /app/

# Instalando angular y dejando en produccion
RUN sudo npm install -g @angular/cli@latest
RUN sudo npm install --save-dev @angular-devkit/build-angular
RUN  sudo npm run build -- --prod

# Instalando imagen nginx y haciendo las configuraciones web
RUN sudo apt install nginx -y
COPY /dist/evaluacion /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf

# Definir puerto
EXPOSE 8080

# Corriendo nginx
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
CMD service nginx start


# Tutorial para desplegar la aplicacion con docker
# sudo docker build . -t evaluacion:latest 
# sudo docker run -d -p 8080:80 evaluacion:latest